$(document).ready(function(){
    
    $("#submitArtist").click(function(){
        return getSpotifyData();
    });
    
});

function getSpotifyData(){
    var artist = $("#artist").val();
    
    if(artist != ''){
        
        
        var clientId = 'a10a24906d70496db1f95ddddab806dc';
        var clientSecret = 'd2ee1e56956846f9bdee03ff07426033';
        var auth = btoa(clientId + ':' + clientSecret);
        
        
        $.ajax({
            url: 'https://accounts.spotify.com/api/token',
            type: 'POST',
            data: 'grant_type=client_credentials',
            beforeSend: function (xhr) {
                xhr.setRequestHeader('Authorization', 'Basic ' + auth);
            },
            success: function(tokenResponse){
                $.ajax({
                    url: 'https://api.spotify.com/v1/search?q=' + artist + '&type=artist',
                    type: 'GET',
                    beforeSend: function(xhr) {
                        xhr.setRequestHeader('Authorization', 'Bearer ' + tokenResponse.access_token);
                    },
                    success: function(data){
                        
                        console.log(data); 
                        var artistInfo = '<h2 style="font-weight:bold; font-size:30px; margin-top:20px;color: white;">Información del Artista: ' + artist + '</h2>';
                        
                       
                        if (data.artists && data.artists.items.length > 0) {
                            
                            var numberOfArtistsToShow = Math.min(data.artists.items.length, 3);

                            for (var i = 0; i < numberOfArtistsToShow; i++) {
                                var currentArtist = data.artists.items[i];
                                artistInfo += "<p style='color: white;'>Artist Name: " + currentArtist.name + "</p>";
                                artistInfo += "<p style='color: white;'>Genre: " + currentArtist.genres.join(', ') + "</p>";
                                artistInfo += "<p style='color: white;'>Popularity: " + currentArtist.popularity + "</p>";
                                artistInfo += "<p style='color: white;'>Followers: " + currentArtist.followers.total + "</p>";
                                artistInfo += "<p style='color: white;'>External URL: <a href='" + currentArtist.external_urls.spotify + "' target='_blank'>" + currentArtist.external_urls.spotify + "</a></p>";
                                
                                
                                getAlbumImages(currentArtist.id, tokenResponse.access_token);
                            }
                        } else {
                            artistInfo += "<p>No artist information found for '" + artist + "'</p>";
                        }
                        
                        $("#spotifyData").html(artistInfo);
                        $("#artist").val('');
                    },
                    error: function(xhr, status, error){
                        
                        console.error(xhr.responseText); 
                        console.error(status); 
                        console.error(error); 

                        $("#error").html("<div class='alert alert-danger' id='errorArtist'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>Error fetching artist information. Check the console for details.</div>");
                    }
                });
            },
            error: function(xhr, status, error){
                
                console.error(xhr.responseText); 
                console.error(status); 
                console.error(error); 

                $("#error").html("<div class='alert alert-danger' id='errorAuth'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>Error authenticating with Spotify API. Check the console for details.</div>");
            }
        });
        
    } else {
        $("#error").html("<div class='alert alert-danger' id='errorArtist'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>Artist field cannot be empty</div>");
    }
}


function getAlbumImages(artistId, accessToken) {
    $.ajax({
        url: 'https://api.spotify.com/v1/artists/' + artistId + '/albums',
        type: 'GET',
        beforeSend: function(xhr) {
            xhr.setRequestHeader('Authorization', 'Bearer ' + accessToken);
        },
        success: function(albumData){
            console.log(albumData); 
            var albumImages = '<h2 style="font-weight:bold; font-size:30px; margin-top:20px; color: white;">Imagenes Album</h2>';
            
            
            if (albumData.items && albumData.items.length > 0) {
                
                var numberOfAlbumsToShow = Math.min(albumData.items.length, 3);

                for (var i = 0; i < numberOfAlbumsToShow; i++) {
                    var album = albumData.items[i];
                    
                    albumImages += '<img src="' + album.images[0].url + '" alt="' + album.name + '" style="width: 200px; height: 200px; margin: 10px;">';
                }
            } else {
                albumImages += '<p>No album information found for this artist.</p>';
            }

            $("#spotifyData").append(albumImages);
        },
        error: function(xhr, status, error){
            
            console.error(xhr.responseText); 
            console.error(status); 
            console.error(error); 
            $("#error").html("<div class='alert alert-danger' id='errorAlbums'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>Error fetching album information. Check the console for details.</div>");
        }
    });
}
